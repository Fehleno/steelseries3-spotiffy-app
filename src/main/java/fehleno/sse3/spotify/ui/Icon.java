/************************************************************************
 * Spotiffy by Fehleno & EnderCrypt                                     *
 * Copyright (C) 2021                                                   *
 *                                                                      *
 * This program is free software: you can redistribute it and/or modify *
 * it under the terms of the GNU General Public License as published by *
 * the Free Software Foundation, either version 3 of the License, or    *
 * (at your option) any later version.                                  *
 *                                                                      *
 * This program is distributed in the hope that it will be useful,      *
 * but WITHOUT ANY WARRANTY; without even the implied warranty of       *
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the        *
 * GNU General Public License for more details.                         *
 *                                                                      *
 * You should have received a copy of the GNU General Public License    *
 * along with this program in its root directory.                       *
 * If not, see <https://www.gnu.org/licenses/gpl-3.0>.                  *
 ************************************************************************/

package fehleno.sse3.spotify.ui;


import java.awt.AWTException;
import java.awt.SystemTray;
import java.awt.TrayIcon;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.nio.file.Paths;

import javax.imageio.ImageIO;

import fehleno.sse3.spotify.utility.Resources;


public class Icon
{
	private final Ui ui;
	private final BufferedImage image;
	private final TrayIcon trayIcon;
	
	public Icon(Ui ui) throws IOException, AWTException
	{
		this.ui = ui;
		byte[] bytes = Resources.readAsBytes(Paths.get("Spotiffy_Small.png"));
		image = ImageIO.read(new ByteArrayInputStream(bytes));
		trayIcon = new TrayIcon(image, "SSE3 - Spotiffy");
		trayIcon.setImageAutoSize(true);
		trayIcon.addMouseListener(new IconMouseListener());
		SystemTray.getSystemTray().add(trayIcon);
		trayIcon.setPopupMenu(new TrayIconMenu(ui));
	}
	
	public BufferedImage getImage()
	{
		return image;
	}
	
	public TrayIcon getTrayIcon()
	{
		return trayIcon;
	}
	
	private class IconMouseListener implements MouseListener
	{
		
		@Override
		public void mouseClicked(MouseEvent e)
		{
			// TODO Auto-generated method stub
			
		}
		
		@Override
		public void mousePressed(MouseEvent e)
		{
			if (e.getClickCount() >= 2)
			{
				ui.getMainWindow().setVisible(true);
			}
			
		}
		
		@Override
		public void mouseReleased(MouseEvent e)
		{
			// TODO Auto-generated method stub
			
		}
		
		@Override
		public void mouseEntered(MouseEvent e)
		{
			// TODO Auto-generated method stub
			
		}
		
		@Override
		public void mouseExited(MouseEvent e)
		{
			// TODO Auto-generated method stub
			
		}
		
	}
}
