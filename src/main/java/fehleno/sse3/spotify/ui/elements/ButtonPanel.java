/************************************************************************
 * Spotiffy by Fehleno & EnderCrypt                                     *
 * Copyright (C) 2021                                                   *
 *                                                                      *
 * This program is free software: you can redistribute it and/or modify *
 * it under the terms of the GNU General Public License as published by *
 * the Free Software Foundation, either version 3 of the License, or    *
 * (at your option) any later version.                                  *
 *                                                                      *
 * This program is distributed in the hope that it will be useful,      *
 * but WITHOUT ANY WARRANTY; without even the implied warranty of       *
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the        *
 * GNU General Public License for more details.                         *
 *                                                                      *
 * You should have received a copy of the GNU General Public License    *
 * along with this program in its root directory.                       *
 * If not, see <https://www.gnu.org/licenses/gpl-3.0>.                  *
 ************************************************************************/

package fehleno.sse3.spotify.ui.elements;


import java.awt.FlowLayout;

import javax.swing.JPanel;

import fehleno.sse3.spotify.ui.Ui;


@SuppressWarnings("serial")
public class ButtonPanel extends JPanel
{
	
	public ButtonPanel(Ui ui)
	{
		setLayout(new FlowLayout());
		add(new RepositoryButton());
		add(new AboutButton());
		add(new SettingsButton(ui));
		add(new HideButton(ui));
	}
}
