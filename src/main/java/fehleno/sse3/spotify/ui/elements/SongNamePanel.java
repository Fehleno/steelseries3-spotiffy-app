/************************************************************************
 * Spotiffy by Fehleno & EnderCrypt                                     *
 * Copyright (C) 2021                                                   *
 *                                                                      *
 * This program is free software: you can redistribute it and/or modify *
 * it under the terms of the GNU General Public License as published by *
 * the Free Software Foundation, either version 3 of the License, or    *
 * (at your option) any later version.                                  *
 *                                                                      *
 * This program is distributed in the hope that it will be useful,      *
 * but WITHOUT ANY WARRANTY; without even the implied warranty of       *
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the        *
 * GNU General Public License for more details.                         *
 *                                                                      *
 * You should have received a copy of the GNU General Public License    *
 * along with this program in its root directory.                       *
 * If not, see <https://www.gnu.org/licenses/gpl-3.0>.                  *
 ************************************************************************/

package fehleno.sse3.spotify.ui.elements;


import java.awt.Dimension;

import javax.swing.BoxLayout;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import fehleno.sse3.spotify.ui.Ui;


@SuppressWarnings("serial")
public class SongNamePanel extends JPanel
{
	private final SongNameLabel songName = new SongNameLabel();
	public final Ui ui;
	
	public SongNamePanel(Ui ui)
	{
		this.ui = ui;
		setBorder(new EmptyBorder(10, 10, 10, 10));
		setLayout(new BoxLayout(this, BoxLayout.Y_AXIS));
		
		JLabel label = new JLabel("Current song:", JLabel.CENTER);
		add(label);
		label.setAlignmentX(CENTER_ALIGNMENT);
		
		songName.setAlignmentX(CENTER_ALIGNMENT);
		add(songName);
		
		setMinimumSize(new Dimension(100, 75));
	}
	
	public void setSongName(String songName)
	{
		this.songName.setSongName(songName);
	}
}
