/************************************************************************
 * Spotiffy by Fehleno & EnderCrypt                                     *
 * Copyright (C) 2021                                                   *
 *                                                                      *
 * This program is free software: you can redistribute it and/or modify *
 * it under the terms of the GNU General Public License as published by *
 * the Free Software Foundation, either version 3 of the License, or    *
 * (at your option) any later version.                                  *
 *                                                                      *
 * This program is distributed in the hope that it will be useful,      *
 * but WITHOUT ANY WARRANTY; without even the implied warranty of       *
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the        *
 * GNU General Public License for more details.                         *
 *                                                                      *
 * You should have received a copy of the GNU General Public License    *
 * along with this program in its root directory.                       *
 * If not, see <https://www.gnu.org/licenses/gpl-3.0>.                  *
 ************************************************************************/

package fehleno.sse3.spotify.ui.elements;


import java.awt.Dimension;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import javax.swing.JLabel;

import fehleno.sse3.spotify.animator.Animator;
import fehleno.sse3.spotify.settings.SettingsManager;


@SuppressWarnings("serial")
public class SongNameLabel extends JLabel
{
	private final ExecutorService executor = Executors.newCachedThreadPool();
	private final Animator animator = new Animator(35, ' ');
	
	public SongNameLabel()
	{
		super("Loading...", JLabel.CENTER);
		animator.setLoopAmount(-1);
		animator.setText(getText());
		setMinimumSize(new Dimension(300, 30));
		executor.execute(this::animationLoop);
	}
	
	private void animationLoop()
	{
		try
		{
			while (true)
			{
				animator.animate();
				setText(animator.getAnimatedText());
				SongNameLabel.this.repaint();
				Thread.sleep(SettingsManager.read().getScrollSpeedDelay());
			}
		}
		catch (InterruptedException e)
		{
			return;
		}
	}
	
	public void setSongName(String name)
	{
		animator.setText(name);
	}
}
