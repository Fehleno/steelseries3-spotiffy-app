/************************************************************************
 * Spotiffy by Fehleno & EnderCrypt                                     *
 * Copyright (C) 2021                                                   *
 *                                                                      *
 * This program is free software: you can redistribute it and/or modify *
 * it under the terms of the GNU General Public License as published by *
 * the Free Software Foundation, either version 3 of the License, or    *
 * (at your option) any later version.                                  *
 *                                                                      *
 * This program is distributed in the hope that it will be useful,      *
 * but WITHOUT ANY WARRANTY; without even the implied warranty of       *
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the        *
 * GNU General Public License for more details.                         *
 *                                                                      *
 * You should have received a copy of the GNU General Public License    *
 * along with this program in its root directory.                       *
 * If not, see <https://www.gnu.org/licenses/gpl-3.0>.                  *
 ************************************************************************/

package fehleno.sse3.spotify.ui;


import java.awt.MenuItem;
import java.awt.PopupMenu;


@SuppressWarnings("serial")
public class TrayIconMenu extends PopupMenu
{
	
	public TrayIconMenu(Ui ui)
	{
		MenuItem open = new MenuItem("Open");
		add(open);
		open.addActionListener((e) -> {
			ui.getMainWindow().setVisible(true);
			ui.getMainWindow().toFront();
			ui.getMainWindow().requestFocus();
		});
		MenuItem exit = new MenuItem("Exit");
		exit.addActionListener((e) -> System.exit(0));
		add(exit);
	}
	
}
