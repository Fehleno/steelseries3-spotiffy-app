/************************************************************************
 * Spotiffy by Fehleno & EnderCrypt                                     *
 * Copyright (C) 2021                                                   *
 *                                                                      *
 * This program is free software: you can redistribute it and/or modify *
 * it under the terms of the GNU General Public License as published by *
 * the Free Software Foundation, either version 3 of the License, or    *
 * (at your option) any later version.                                  *
 *                                                                      *
 * This program is distributed in the hope that it will be useful,      *
 * but WITHOUT ANY WARRANTY; without even the implied warranty of       *
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the        *
 * GNU General Public License for more details.                         *
 *                                                                      *
 * You should have received a copy of the GNU General Public License    *
 * along with this program in its root directory.                       *
 * If not, see <https://www.gnu.org/licenses/gpl-3.0>.                  *
 ************************************************************************/

package fehleno.sse3.spotify.ui;


import java.awt.AWTException;
import java.io.IOException;

import fehleno.sse3.spotify.settings.ui.SettingsWindow;


public class Ui
{
	private final MainWindow mainWindow;
	private final SettingsWindow settingsWindow;
	private final Icon icon;
	
	public Ui() throws IOException, AWTException
	{
		icon = new Icon(this);
		mainWindow = new MainWindow(this);
		mainWindow.setIconImage(icon.getImage());
		settingsWindow = new SettingsWindow(mainWindow);
	}
	
	public MainWindow getMainWindow()
	{
		return mainWindow;
	}
	
	public SettingsWindow getSettingsWindow()
	{
		return settingsWindow;
	}
	
	public Icon getIcon()
	{
		
		return icon;
	}
	
}
