/************************************************************************
 * Spotiffy by Fehleno & EnderCrypt                                     *
 * Copyright (C) 2021                                                   *
 *                                                                      *
 * This program is free software: you can redistribute it and/or modify *
 * it under the terms of the GNU General Public License as published by *
 * the Free Software Foundation, either version 3 of the License, or    *
 * (at your option) any later version.                                  *
 *                                                                      *
 * This program is distributed in the hope that it will be useful,      *
 * but WITHOUT ANY WARRANTY; without even the implied warranty of       *
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the        *
 * GNU General Public License for more details.                         *
 *                                                                      *
 * You should have received a copy of the GNU General Public License    *
 * along with this program in its root directory.                       *
 * If not, see <https://www.gnu.org/licenses/gpl-3.0>.                  *
 ************************************************************************/

package fehleno.sse3.spotify.ui;


import java.awt.Dimension;

import javax.swing.BoxLayout;
import javax.swing.JFrame;

import fehleno.sse3.spotify.Information;
import fehleno.sse3.spotify.ui.elements.ButtonPanel;
import fehleno.sse3.spotify.ui.elements.SongNamePanel;


@SuppressWarnings("serial")
public class MainWindow extends JFrame
{
	private final SongNamePanel songNamePanel;
	
	public MainWindow(Ui ui)
	{
		songNamePanel = new SongNamePanel(ui);
		setTitle(Information.name + " for SS3 V" + Information.version);
		setMinimumSize(new Dimension(300, 110));
		setLayout(new BoxLayout(getContentPane(), BoxLayout.Y_AXIS));
		add(songNamePanel);
		add(new ButtonPanel(ui));
		pack();
		setResizable(false);
		setIconImage(getIconImage());
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setVisible(true);
	}
	
	public SongNamePanel getSongNamePanel()
	{
		return songNamePanel;
	}
	
	@Override
	public void pack()
	{
		super.pack();
		setLocationRelativeTo(null);
	}
}
