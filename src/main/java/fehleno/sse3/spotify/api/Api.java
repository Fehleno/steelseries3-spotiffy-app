/************************************************************************
 * Spotiffy by Fehleno & EnderCrypt                                     *
 * Copyright (C) 2021                                                   *
 *                                                                      *
 * This program is free software: you can redistribute it and/or modify *
 * it under the terms of the GNU General Public License as published by *
 * the Free Software Foundation, either version 3 of the License, or    *
 * (at your option) any later version.                                  *
 *                                                                      *
 * This program is distributed in the hope that it will be useful,      *
 * but WITHOUT ANY WARRANTY; without even the implied warranty of       *
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the        *
 * GNU General Public License for more details.                         *
 *                                                                      *
 * You should have received a copy of the GNU General Public License    *
 * along with this program in its root directory.                       *
 * If not, see <https://www.gnu.org/licenses/gpl-3.0>.                  *
 ************************************************************************/

package fehleno.sse3.spotify.api;


import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.jsoup.Connection;
import org.jsoup.Connection.Method;
import org.jsoup.Connection.Response;
import org.jsoup.Jsoup;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;


public class Api
{
	private static final Logger logger = LogManager.getLogger(Api.class);
	private static final Gson gson = new GsonBuilder().setPrettyPrinting().create();
	
	private final String name;
	private final String host;
	
	public Api(String name) throws IOException
	{
		this.name = name;
		Path path = Paths.get(System.getenv("PROGRAMDATA")).resolve("SteelSeries/SteelSeries Engine 3/coreProps.json");
		logger.info("Reading core props from path: " + path);
		String contents = new String(Files.readAllBytes(path));
		logger.info("Read core props: \n" + contents);
		JsonObject root = gson.fromJson(contents, JsonObject.class);
		host = root.get("address").getAsString();
		logger.info("Address: " + host);
	}
	
	public JsonObject send(String endpoint, JsonObject json) throws IOException
	{
		json.addProperty("game", name);
		Connection connection = Jsoup.connect("http://" + host + "/" + endpoint);
		connection.header("Accept", "application/json");
		connection.header("Content-Type", "application/json");
		connection.method(Method.POST);
		connection.ignoreHttpErrors(true);
		String sendContents = gson.toJson(json);
		logger.trace("Sent: " + sendContents);
		connection.requestBody(sendContents);
		Response response = connection.execute();
		String receivedContents = response.body();
		JsonObject responseJson = gson.fromJson(receivedContents, JsonObject.class);
		logger.trace("Code: " + response.statusCode());
		logger.trace("Received: " + receivedContents);
		if (response.statusCode() != 200)
		{
			throw new ApiException(responseJson.get("error").getAsString());
		}
		return responseJson;
	}
	
	public JsonObject bindText(String event) throws IOException
	{
		JsonObject root = new JsonObject();
		root.add("handlers", gson.fromJson("[{'device-type': 'screened', 'mode': 'screen', 'zone': 'one', 'datas': [{'has-text': True, length-millis': 2000}]}]", JsonArray.class));
		root.addProperty("event", event);
		return send("bind_game_event", root);
	}
	
	public JsonObject setText(String event, String text) throws IOException
	{
		JsonObject root = new JsonObject();
		root.addProperty("event", event);
		JsonObject data = new JsonObject();
		data.addProperty("value", text);
		root.add("data", data);
		return send("game_event", root);
	}
	
	public void registerMetaData(String name, String developer) throws IOException
	{
		JsonObject json = new JsonObject();
		json.addProperty("game_display_name", name);
		json.addProperty("developer", developer);
		json.addProperty("deinitialize_timer_length_ms", 10_000);
		send("game_metadata", json);
	}
	
	public void removeApp() throws IOException
	{
		JsonObject json = new JsonObject();
		send("remove_game", json);
	}
	
	public void ping() throws IOException
	{
		JsonObject json = new JsonObject();
		send("game_heartbeat", json);
	}
}
