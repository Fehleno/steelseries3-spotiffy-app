/************************************************************************
 * Spotiffy by Fehleno & EnderCrypt                                     *
 * Copyright (C) 2021                                                   *
 *                                                                      *
 * This program is free software: you can redistribute it and/or modify *
 * it under the terms of the GNU General Public License as published by *
 * the Free Software Foundation, either version 3 of the License, or    *
 * (at your option) any later version.                                  *
 *                                                                      *
 * This program is distributed in the hope that it will be useful,      *
 * but WITHOUT ANY WARRANTY; without even the implied warranty of       *
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the        *
 * GNU General Public License for more details.                         *
 *                                                                      *
 * You should have received a copy of the GNU General Public License    *
 * along with this program in its root directory.                       *
 * If not, see <https://www.gnu.org/licenses/gpl-3.0>.                  *
 ************************************************************************/

package fehleno.sse3.spotify.utility;


import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;


public class Directories
{
	private static final Logger logger = LogManager.getLogger(Directories.class);
	
	private static List<Path> paths = new ArrayList<>();
	
	public static final Path SS3SPOTIFFY = create(System.getenv("APPDATA") + "/SS3Spotiffy");
	
	private static Path create(String path)
	{
		Path pathObject = Paths.get(path);
		paths.add(pathObject);
		return pathObject;
	}
	
	@SuppressWarnings("unused")
	private static Path create(Path parent, String path)
	{
		return create(parent.resolve(path).toString());
	}
	
	public static void generate()
	{
		try
		{
			for (Path path : paths)
			{
				if (Files.exists(path))
				{
					if (Files.isDirectory(path) == false)
					{
						throw new IllegalArgumentException(path + " is suppose to be a directory, but is a file");
					}
				}
				else
				{
					logger.debug("Creating directory: " + path);
					Files.createDirectories(path);
				}
			}
		}
		catch (IOException e)
		{
			logger.error("Failed to setup directories", e);
			System.exit(1);
		}
	}
}
