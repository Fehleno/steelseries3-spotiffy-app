/************************************************************************
 * Spotiffy by Fehleno & EnderCrypt                                     *
 * Copyright (C) 2021                                                   *
 *                                                                      *
 * This program is free software: you can redistribute it and/or modify *
 * it under the terms of the GNU General Public License as published by *
 * the Free Software Foundation, either version 3 of the License, or    *
 * (at your option) any later version.                                  *
 *                                                                      *
 * This program is distributed in the hope that it will be useful,      *
 * but WITHOUT ANY WARRANTY; without even the implied warranty of       *
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the        *
 * GNU General Public License for more details.                         *
 *                                                                      *
 * You should have received a copy of the GNU General Public License    *
 * along with this program in its root directory.                       *
 * If not, see <https://www.gnu.org/licenses/gpl-3.0>.                  *
 ************************************************************************/

package fehleno.sse3.spotify.utility;


import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Path;
import java.util.Arrays;
import java.util.List;
import java.util.Objects;

import org.apache.commons.io.IOUtils;


public class Resources
{
	public interface InputStreamConverter<T>
	{
		public T convert(InputStream stream) throws IOException;
	}
	
	public static <T> T readAs(Path path, InputStreamConverter<T> converter) throws IOException
	{
		Objects.requireNonNull(converter);
		
		String jarPath = "/" + path;
		try (InputStream input = Resources.class.getResourceAsStream(jarPath.toString()))
		{
			if (input == null)
			{
				throw new NoSuchFieldError("src/main/resources" + jarPath);
			}
			return converter.convert(input);
		}
	}
	
	public static byte[] readAsBytes(Path path) throws IOException
	{
		return readAs(path, IOUtils::toByteArray);
	}
	
	public static String readAsString(Path path) throws IOException
	{
		return new String(readAsBytes(path));
	}
	
	public static List<String> readAsLines(Path path) throws IOException
	{
		return Arrays.asList(readAsString(path).split("\n"));
	}
}
