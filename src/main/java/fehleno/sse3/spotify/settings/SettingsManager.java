/************************************************************************
 * Spotiffy by Fehleno & EnderCrypt                                     *
 * Copyright (C) 2021                                                   *
 *                                                                      *
 * This program is free software: you can redistribute it and/or modify *
 * it under the terms of the GNU General Public License as published by *
 * the Free Software Foundation, either version 3 of the License, or    *
 * (at your option) any later version.                                  *
 *                                                                      *
 * This program is distributed in the hope that it will be useful,      *
 * but WITHOUT ANY WARRANTY; without even the implied warranty of       *
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the        *
 * GNU General Public License for more details.                         *
 *                                                                      *
 * You should have received a copy of the GNU General Public License    *
 * along with this program in its root directory.                       *
 * If not, see <https://www.gnu.org/licenses/gpl-3.0>.                  *
 ************************************************************************/

package fehleno.sse3.spotify.settings;


import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;

import javax.swing.JOptionPane;

import org.yaml.snakeyaml.DumperOptions.FlowStyle;
import org.yaml.snakeyaml.Yaml;
import org.yaml.snakeyaml.introspector.BeanAccess;
import org.yaml.snakeyaml.nodes.Tag;

import fehleno.sse3.spotify.utility.Directories;


public class SettingsManager
{
	private static Yaml yaml = new Yaml();
	private static Path path = Directories.SS3SPOTIFFY.resolve("settings.yaml");
	static
	{
		yaml.setBeanAccess(BeanAccess.FIELD);
	}
	
	public synchronized static Settings read()
	{
		if (Files.exists(path) == false)
		{
			Write(new Settings());
		}
		try
		{
			byte[] bytes = Files.readAllBytes(path);
			
			String string = new String(bytes);
			
			Settings settings = yaml.loadAs(string, Settings.class);
			return settings;
		}
		catch (IOException e)
		{
			e.printStackTrace();
			JOptionPane.showMessageDialog(null, "Failed to read from the settings file", "Settings read error", JOptionPane.ERROR_MESSAGE);
			System.exit(1);
			return null;
		}
	}
	
	public synchronized static void Write(Settings settings)
	{
		String string = yaml.dumpAs(settings, Tag.MAP, FlowStyle.BLOCK);
		byte[] bytes = string.getBytes();
		try
		{
			Files.write(path, bytes);
		}
		catch (IOException e)
		{
			e.printStackTrace();
			JOptionPane.showMessageDialog(null, "Failed to write to the settings file", "Settings write error", JOptionPane.ERROR_MESSAGE);
			System.exit(1);
		}
	}
}
