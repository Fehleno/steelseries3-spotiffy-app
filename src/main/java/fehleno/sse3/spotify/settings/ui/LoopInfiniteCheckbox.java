/************************************************************************
 * Spotiffy by Fehleno & EnderCrypt                                     *
 * Copyright (C) 2021                                                   *
 *                                                                      *
 * This program is free software: you can redistribute it and/or modify *
 * it under the terms of the GNU General Public License as published by *
 * the Free Software Foundation, either version 3 of the License, or    *
 * (at your option) any later version.                                  *
 *                                                                      *
 * This program is distributed in the hope that it will be useful,      *
 * but WITHOUT ANY WARRANTY; without even the implied warranty of       *
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the        *
 * GNU General Public License for more details.                         *
 *                                                                      *
 * You should have received a copy of the GNU General Public License    *
 * along with this program in its root directory.                       *
 * If not, see <https://www.gnu.org/licenses/gpl-3.0>.                  *
 ************************************************************************/

package fehleno.sse3.spotify.settings.ui;


import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;

import javax.swing.JCheckBox;

import fehleno.sse3.spotify.settings.SettingsManager;


@SuppressWarnings("serial")
public class LoopInfiniteCheckbox extends JCheckBox implements ItemListener, SettingsElement
{
	private final SettingsWindow settingsWindow;
	
	public LoopInfiniteCheckbox(SettingsWindow settingsWindow)
	{
		super();
		this.settingsWindow = settingsWindow;
		addItemListener(this);
	}
	
	@Override
	public void itemStateChanged(ItemEvent e)
	{
		boolean selected = (e.getStateChange() == ItemEvent.SELECTED);
		updateUi(selected);
	}
	
	private void updateUi(boolean selected)
	{
		getModel().setSelected(selected);
		settingsWindow.getLoopAmountUi().setEnabled(selected == false);
		settingsWindow.getSettings().setInfiniteLoop(selected);
	}
	
	@Override
	public void loadSettings()
	{
		updateUi(SettingsManager.read().isInfiniteLoop());
	}
	
}
