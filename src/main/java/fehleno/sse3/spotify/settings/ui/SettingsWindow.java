/************************************************************************
 * Spotiffy by Fehleno & EnderCrypt                                     *
 * Copyright (C) 2021                                                   *
 *                                                                      *
 * This program is free software: you can redistribute it and/or modify *
 * it under the terms of the GNU General Public License as published by *
 * the Free Software Foundation, either version 3 of the License, or    *
 * (at your option) any later version.                                  *
 *                                                                      *
 * This program is distributed in the hope that it will be useful,      *
 * but WITHOUT ANY WARRANTY; without even the implied warranty of       *
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the        *
 * GNU General Public License for more details.                         *
 *                                                                      *
 * You should have received a copy of the GNU General Public License    *
 * along with this program in its root directory.                       *
 * If not, see <https://www.gnu.org/licenses/gpl-3.0>.                  *
 ************************************************************************/

package fehleno.sse3.spotify.settings.ui;


import java.awt.Component;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;
import java.util.ArrayList;
import java.util.List;

import javax.swing.BorderFactory;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JLabel;

import fehleno.sse3.spotify.settings.Settings;
import fehleno.sse3.spotify.settings.SettingsManager;


public class SettingsWindow extends JDialog
{
	private JFrame parent;
	private Settings settings;
	
	private List<SettingsElement> elements = new ArrayList<>();
	private LoopAmountUi loopAmountUi;
	
	public SettingsWindow(JFrame parent)
	{
		super(parent);
		this.parent = parent;
		setTitle("Settings");
		getRootPane().setBorder(BorderFactory.createEmptyBorder(5, 5, 5, 5));
		GridBagLayout layout = new GridBagLayout();
		getContentPane().setLayout(layout);
		
		GridBagConstraints constraints = new GridBagConstraints();
		
		constraints.gridx = 0;
		constraints.gridy = 0;
		add(new JLabel("Scroll Speed: "), constraints);
		
		constraints.gridx = 1;
		constraints.gridy = 0;
		add(new ScrollSpeedUi(this), constraints);
		
		constraints.gridx = 0;
		constraints.gridy = 1;
		add(new JLabel("Loop Amount: "), constraints);
		
		constraints.gridx = 1;
		constraints.gridy = 1;
		loopAmountUi = new LoopAmountUi(this);
		add(loopAmountUi, constraints);
		
		constraints.gridx = 0;
		constraints.gridy = 2;
		add(new JLabel("Infinite Loop: "), constraints);
		
		constraints.gridx = 1;
		constraints.gridy = 2;
		add(new LoopInfiniteCheckbox(this), constraints);
		
		constraints.gridx = 1;
		constraints.gridy = 3;
		add(new SaveButton(this), constraints);
		
		pack();
		setResizable(false);
		setVisible(false);
		setDefaultCloseOperation(DO_NOTHING_ON_CLOSE);
		addWindowListener(new InternalWindowListener());
	}
	
	public LoopAmountUi getLoopAmountUi()
	{
		return loopAmountUi;
	}
	
	@Override
	public void add(Component comp, Object constraints)
	{
		if (comp instanceof SettingsElement)
		{
			elements.add((SettingsElement) comp);
		}
		super.add(comp, constraints);
	}
	
	public Settings getSettings()
	{
		return settings;
	}
	
	public void close(boolean save)
	{
		if (save)
		{
			SettingsManager.Write(settings);
		}
		settings = null;
		setVisible(false);
	}
	
	public void open()
	{
		settings = SettingsManager.read();
		elements.forEach(e -> e.loadSettings());
		setLocationRelativeTo(null);
		setVisible(true);
	}
	
	@Override
	public void pack()
	{
		super.pack();
		setLocationRelativeTo(null);
	}
	
	private class InternalWindowListener implements WindowListener
	{
		
		@Override
		public void windowOpened(WindowEvent e)
		{
			// TODO Auto-generated method stub
			
		}
		
		@Override
		public void windowClosing(WindowEvent e)
		{
			close(false);
			
		}
		
		@Override
		public void windowClosed(WindowEvent e)
		{
			// TODO Auto-generated method stub
			
		}
		
		@Override
		public void windowIconified(WindowEvent e)
		{
			// TODO Auto-generated method stub
			
		}
		
		@Override
		public void windowDeiconified(WindowEvent e)
		{
			// TODO Auto-generated method stub
			
		}
		
		@Override
		public void windowActivated(WindowEvent e)
		{
			// TODO Auto-generated method stub
			
		}
		
		@Override
		public void windowDeactivated(WindowEvent e)
		{
			// TODO Auto-generated method stub
			
		}
	}
}
