/************************************************************************
 * Spotiffy by Fehleno & EnderCrypt                                     *
 * Copyright (C) 2021                                                   *
 *                                                                      *
 * This program is free software: you can redistribute it and/or modify *
 * it under the terms of the GNU General Public License as published by *
 * the Free Software Foundation, either version 3 of the License, or    *
 * (at your option) any later version.                                  *
 *                                                                      *
 * This program is distributed in the hope that it will be useful,      *
 * but WITHOUT ANY WARRANTY; without even the implied warranty of       *
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the        *
 * GNU General Public License for more details.                         *
 *                                                                      *
 * You should have received a copy of the GNU General Public License    *
 * along with this program in its root directory.                       *
 * If not, see <https://www.gnu.org/licenses/gpl-3.0>.                  *
 ************************************************************************/

package fehleno.sse3.spotify.settings.ui;


import java.awt.Dimension;

import javax.swing.JFormattedTextField;
import javax.swing.JSpinner;
import javax.swing.SpinnerNumberModel;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;
import javax.swing.text.DefaultFormatter;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;


@SuppressWarnings("serial")
public abstract class SettingsNumberInput extends JSpinner implements SettingsElement
{
	private static final Logger logger = LogManager.getLogger(SettingsNumberInput.class);
	
	protected final SettingsWindow settingsWindow;
	protected final JFormattedTextField textField;
	
	public SettingsNumberInput(SettingsWindow settingsWindow, SpinnerNumberModel spinnerNumberModel)
	{
		super(spinnerNumberModel);
		this.settingsWindow = settingsWindow;
		
		textField = ((JSpinner.DefaultEditor) getEditor()).getTextField();
		textField.setEditable(false);
		DefaultFormatter formatter = (DefaultFormatter) textField.getFormatter();
		formatter.setCommitsOnValidEdit(false);
		textField.getDocument().addDocumentListener(new InternalDocumentListener());
		setPreferredSize(new Dimension(30, 20));
	}
	
	public abstract void valueChanged(int value);
	
	private class InternalDocumentListener implements DocumentListener
	{
		public void onChange()
		{
			if (settingsWindow.getSettings() != null)
			{
				logger.trace(textField.getText());
				valueChanged(Integer.parseInt(textField.getText()));
			}
			
		}
		
		@Override
		public void insertUpdate(DocumentEvent e)
		{
			onChange();
			
		}
		
		@Override
		public void removeUpdate(DocumentEvent e)
		{
			//onChange();
			
		}
		
		@Override
		public void changedUpdate(DocumentEvent e)
		{
			onChange();
		}
		
	}
}
