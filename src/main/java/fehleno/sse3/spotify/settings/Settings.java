/************************************************************************
 * Spotiffy by Fehleno & EnderCrypt                                     *
 * Copyright (C) 2021                                                   *
 *                                                                      *
 * This program is free software: you can redistribute it and/or modify *
 * it under the terms of the GNU General Public License as published by *
 * the Free Software Foundation, either version 3 of the License, or    *
 * (at your option) any later version.                                  *
 *                                                                      *
 * This program is distributed in the hope that it will be useful,      *
 * but WITHOUT ANY WARRANTY; without even the implied warranty of       *
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the        *
 * GNU General Public License for more details.                         *
 *                                                                      *
 * You should have received a copy of the GNU General Public License    *
 * along with this program in its root directory.                       *
 * If not, see <https://www.gnu.org/licenses/gpl-3.0>.                  *
 ************************************************************************/

package fehleno.sse3.spotify.settings;

public class Settings
{
	private int scrollSpeed = 3;
	
	public int getScrollSpeed()
	{
		return scrollSpeed;
	}
	
	public void setScrollSpeed(int scrollSpeed)
	{
		this.scrollSpeed = scrollSpeed;
	}
	
	public int getScrollSpeedDelay()
	{
		return (int) (50 * (Math.pow(2, 5 - getScrollSpeed())));
	}
	
	private int loopAmount = 0;
	
	public int getLoopAmount()
	{
		return loopAmount;
	}
	
	public void setLoopAmount(int loopAmount)
	{
		this.loopAmount = loopAmount;
	}
	
	private boolean infiniteLoop = false;
	
	public boolean isInfiniteLoop()
	{
		return infiniteLoop;
	}
	
	public void setInfiniteLoop(boolean infiniteLoop)
	{
		this.infiniteLoop = infiniteLoop;
	}
	
}
