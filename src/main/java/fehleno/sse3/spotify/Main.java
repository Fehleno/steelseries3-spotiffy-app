/************************************************************************
 * Spotiffy by Fehleno & EnderCrypt                                     *
 * Copyright (C) 2021                                                   *
 *                                                                      *
 * This program is free software: you can redistribute it and/or modify *
 * it under the terms of the GNU General Public License as published by *
 * the Free Software Foundation, either version 3 of the License, or    *
 * (at your option) any later version.                                  *
 *                                                                      *
 * This program is distributed in the hope that it will be useful,      *
 * but WITHOUT ANY WARRANTY; without even the implied warranty of       *
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the        *
 * GNU General Public License for more details.                         *
 *                                                                      *
 * You should have received a copy of the GNU General Public License    *
 * along with this program in its root directory.                       *
 * If not, see <https://www.gnu.org/licenses/gpl-3.0>.                  *
 ************************************************************************/

package fehleno.sse3.spotify;


import java.awt.AWTException;
import java.io.IOException;
import java.util.Arrays;
import java.util.List;
import java.util.Objects;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.regex.Pattern;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import fehleno.sse3.spotify.animator.Animator;
import fehleno.sse3.spotify.api.Api;
import fehleno.sse3.spotify.process.ProcessCatcher;
import fehleno.sse3.spotify.process.ProcessEntry;
import fehleno.sse3.spotify.settings.Settings;
import fehleno.sse3.spotify.settings.SettingsManager;
import fehleno.sse3.spotify.ui.Ui;
import fehleno.sse3.spotify.utility.Directories;


public class Main
{
	private static final Logger logger = LogManager.getLogger(Main.class);
	
	private static final ExecutorService executor = Executors.newCachedThreadPool();
	
	private static Api api;
	private static Animator animator = new Animator(20, ' ');
	private static List<Pattern> ignoreList = Arrays.asList(Pattern.compile(".*spotify.*", Pattern.CASE_INSENSITIVE));
	private static Ui ui;
	
	public static void main(String[] args) throws IOException, InterruptedException, AWTException
	{
		Directories.generate();
		
		// UI
		ui = new Ui();
		
		// SteelSeries3 API Setup
		api = new Api("SPOTIFFY");
		api.registerMetaData("Spotify", "Fehleno");
		api.bindText("SONG_NAME");
		
		// Animator Setup
		executor.execute(Main::animate);
		
		// Process Watcher
		String oldText = null;
		
		while (true)
		{
			ProcessCatcher processCatcher = ProcessCatcher.generate();
			ProcessEntry spotify = processCatcher.getProcessByName("Spotify").stream().findFirst().orElse(null);
			if (spotify != null)
			{
				String text = spotify.getTitle().trim();
				if (ignoreList.stream().anyMatch(p -> p.matcher(text).find()))
				{
					continue;
				}
				if (Objects.equals(oldText, text) == false)
				{
					ui.getMainWindow().getSongNamePanel().setSongName(text);
					animator.setText("♪ " + text + " ♫");
					oldText = text;
				}
			}
			else
			{
				ui.getMainWindow().getSongNamePanel().setSongName("Spotify not found");
				animator.setText("Spotify not found");
			}
		}
	}
	
	public static void animate()
	{
		try
		{
			while (true)
			{
				Settings settings = SettingsManager.read();
				animator.setLoopAmount(settings.isInfiniteLoop() ? -1 : settings.getLoopAmount());
				if (animator.hasText())
				{
					String text = animator.getAnimatedText();
					animator.animate();
					api.setText("SONG_NAME", text);
				}
				Thread.sleep(settings.getScrollSpeedDelay());
			}
		}
		catch (InterruptedException e)
		{
			return;
		}
		catch (IOException e)
		{
			logger.error("Animator thread crashed", e);
		}
	}
}
