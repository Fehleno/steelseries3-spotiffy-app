/************************************************************************
 * Spotiffy by Fehleno & EnderCrypt                                     *
 * Copyright (C) 2021                                                   *
 *                                                                      *
 * This program is free software: you can redistribute it and/or modify *
 * it under the terms of the GNU General Public License as published by *
 * the Free Software Foundation, either version 3 of the License, or    *
 * (at your option) any later version.                                  *
 *                                                                      *
 * This program is distributed in the hope that it will be useful,      *
 * but WITHOUT ANY WARRANTY; without even the implied warranty of       *
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the        *
 * GNU General Public License for more details.                         *
 *                                                                      *
 * You should have received a copy of the GNU General Public License    *
 * along with this program in its root directory.                       *
 * If not, see <https://www.gnu.org/licenses/gpl-3.0>.                  *
 ************************************************************************/

package fehleno.sse3.spotify.process;


import java.util.regex.Matcher;
import java.util.regex.Pattern;


public class ProcessEntry
{
	private static final Pattern pattern = Pattern.compile(" *(\\d*) ([\\w\\.]*) *(.*) *");
	
	public static ProcessEntry resolve(String line)
	{
		Matcher matcher = pattern.matcher(line);
		if (matcher.find())
		{
			int pid = Integer.parseInt(matcher.group(1));
			String name = matcher.group(2);
			String title = matcher.group(3);
			return new ProcessEntry(pid, name, title);
		}
		else
		{
			throw new IllegalArgumentException("Not a valid process line: " + line);
		}
	}
	
	private final int pid;
	private final String name;
	private final String title;
	
	public ProcessEntry(int pid, String name, String title)
	{
		this.pid = pid;
		this.name = name;
		this.title = title;
	}
	
	public int getPid()
	{
		return pid;
	}
	
	public String getName()
	{
		return name;
	}
	
	public String getTitle()
	{
		return title;
	}
	
}
