/************************************************************************
 * Spotiffy by Fehleno & EnderCrypt                                     *
 * Copyright (C) 2021                                                   *
 *                                                                      *
 * This program is free software: you can redistribute it and/or modify *
 * it under the terms of the GNU General Public License as published by *
 * the Free Software Foundation, either version 3 of the License, or    *
 * (at your option) any later version.                                  *
 *                                                                      *
 * This program is distributed in the hope that it will be useful,      *
 * but WITHOUT ANY WARRANTY; without even the implied warranty of       *
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the        *
 * GNU General Public License for more details.                         *
 *                                                                      *
 * You should have received a copy of the GNU General Public License    *
 * along with this program in its root directory.                       *
 * If not, see <https://www.gnu.org/licenses/gpl-3.0>.                  *
 ************************************************************************/

package fehleno.sse3.spotify.process;


import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;


public class ProcessCatcher
{
	public static ProcessCatcher generate() throws IOException
	{
		Process process = Runtime.getRuntime().exec("powershell.exe Get-Process | Where-Object {$_.mainWindowTitle} | Format-Table Id, Name, mainWindowtitle -AutoSize");
		InputStream input = process.getInputStream();
		ByteArrayOutputStream output = new ByteArrayOutputStream();
		int b;
		while ((b = input.read()) != -1)
		{
			output.write(b);
		}
		String content = new String(output.toByteArray());
		List<ProcessEntry> entries = new ArrayList<>();
		for (String line : content.split("\n"))
		{
			try
			{
				entries.add(ProcessEntry.resolve(line));
			}
			catch (IllegalArgumentException e)
			{
				// Windows clutter
			}
		}
		return new ProcessCatcher(entries);
	}
	
	private List<ProcessEntry> processes;
	
	public ProcessCatcher(List<ProcessEntry> processes)
	{
		this.processes = new ArrayList<>(processes);
	}
	
	public List<ProcessEntry> getProcesses()
	{
		return Collections.unmodifiableList(processes);
	}
	
	public List<ProcessEntry> getProcessByName(String name)
	{
		List<ProcessEntry> result = new ArrayList<>();
		for (ProcessEntry process : processes)
		{
			if (process.getName().equalsIgnoreCase(name))
			{
				result.add(process);
			}
		}
		return result;
	}
	
}
