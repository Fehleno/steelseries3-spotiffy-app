/************************************************************************
 * Spotiffy by Fehleno & EnderCrypt                                     *
 * Copyright (C) 2021                                                   *
 *                                                                      *
 * This program is free software: you can redistribute it and/or modify *
 * it under the terms of the GNU General Public License as published by *
 * the Free Software Foundation, either version 3 of the License, or    *
 * (at your option) any later version.                                  *
 *                                                                      *
 * This program is distributed in the hope that it will be useful,      *
 * but WITHOUT ANY WARRANTY; without even the implied warranty of       *
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the        *
 * GNU General Public License for more details.                         *
 *                                                                      *
 * You should have received a copy of the GNU General Public License    *
 * along with this program in its root directory.                       *
 * If not, see <https://www.gnu.org/licenses/gpl-3.0>.                  *
 ************************************************************************/

package fehleno.sse3.spotify.animator;


import java.util.Objects;


public class Animator
{
	private String text;
	private int index;
	private final int max;
	private final char whiteSpace;
	private int loopAmount = 0;
	private int loopIndex = 0;
	
	public Animator(int max, char whiteSpace)
	{
		this.max = max;
		this.whiteSpace = whiteSpace;
	}
	
	public void setLoopAmount(int loopAmount)
	{
		this.loopAmount = loopAmount;
	}
	
	public char getWhiteSpace()
	{
		return whiteSpace;
	}
	
	public boolean hasText()
	{
		return text != null;
	}
	
	public void setText(String text)
	{
		String newText = text.trim();
		if (Objects.equals(getText(), newText) == false)
		{
			this.text = newText;
			this.index = max;
		}
	}
	
	public void clearText()
	{
		text = null;
		index = 0;
	}
	
	public String getText()
	{
		return text;
	}
	
	public String getAnimatedText()
	{
		if (hasText() == false)
		{
			return null;
		}
		StringBuilder sb = new StringBuilder();
		for (int i = 0; i < max; i++)
		{
			if (i < index)
			{
				sb.append(whiteSpace);
				continue;
			}
			if (i >= index + text.length())
			{
				sb.append(whiteSpace);
				continue;
			}
			int charIndex = i - index;
			char c = text.charAt(charIndex);
			sb.append(c);
		}
		return sb.toString();
	}
	
	public void animate()
	{
		if (hasText())
		{
			index--;
			if (index + text.length() < 0)
			{
				if (loopAmount >= 0 && loopIndex >= loopAmount)
				{
					clearText();
					loopIndex = 0;
				}
				else
				{
					this.index = max;
					loopIndex++;
				}
			}
		}
	}
}
