# Spotiffy V1.0 for SteelSeries3

## Description
Spotiffy is an open source Java powered app that controls the OLED display from SteelSeries devices to display the current Spotify song. V1.0 has been tested on the SteelSeries Arctis Pro Wireless but it should work on every OLED device from SteelSeries. Please report any issues [here](https://gitlab.com/Fehleno/steelseries3-spotiffy-app/-/issues).

## Usage
To get started with Spotiffy, make sure you have JAVA 1.8 or higher installed. Make sure that the SteelSeries  3 is running. Extract the zip file and run the Jar file. Spotiffy will now create a new app entry in the SS3 engine.

Spotiffy can only detect a Spotify song when the Spotify app is in the forground or minimized. Closing Spotify to the system tray will replace the song title with "Spotify not detected".

## Download
You can download the latest stable version of Spotiffy [here](https://gitlab.com/Fehleno/steelseries3-spotiffy-app/-/releases/).

## FAQ
**Q**: Why is the song name cutoff?

**A**: Currently, due to technical limitations, Spotiffy can only load up to 48 characters. 


**Q**: Double clicking the Jar file doesn't launch Spotiffy / Launches decompression software (Like WinZip)?

**A**: Right click the Jar file -> Open with... -> Select Java.


**Q**: Spotiffy is open and a song is being displayed on the app, but nothing is happening on the OLED?

**A**: Make sure that the SteelSeries Engine 3 is running in the background.


**Q**: Where is the settings file located at?

**A**: %APPDATA%/SS3Spotiffy

## Credits
Spotiffy has been created by [Fehleno](https://gitlab.com/Fehleno) & [EnderCrypt](https://gitlab.com/EnderCrypt).
